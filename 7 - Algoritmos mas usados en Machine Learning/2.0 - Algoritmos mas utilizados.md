
# *******************************************************************************************************************************************


# Tipos de algoritmo en ML

* Aprendizaje supervisados. (Supervised Learning)
* Aprendizaje no supervisados. (Unsupervised Learning)
* Aprendizaje reforzado. (Reinforcement Learning)

[VER_img:2.1-aprendizaje-automatico]


# Aprendizaje reforzado

Se puede aplicar a muchos algoritmos de ML, simplemente es tener un resultado final. y ver esto como se comporta en un escenario de uso, como
por ejemplo una ecuacion objetivo. Es decir 'tiene que darme este resultado, se acerco? Rta. No. se alejo? Rta Si. entonces se repite el 
ciclo'. y con esto los algoritmos se pueden ir entrenando automaticamente.

[VER_img:2.2-ml-reinforcement-learning]

En este juego se puede aplicar esto. Cuando pasas un nivel, obtienes una recompensa. Cuando agarras una moneda, entonces estas aprendiendo 
que es bueno, cuando chocas con un enemigo aprendes que es malo chocar con un enemigo porque te baja vida. Asi mismo si derrotas a un jefe,
sabes que es bueno porque ayuda a que seas mas fuerte. Los videojuegos son un claro ejemplo de reforcement-learning. Los diseñadores de juego
lo aplican para que nosotros sepamos como aprender a jugar. Esto mismo aplica con algoritmos de inteligencia artificial.


# Aprendizaje automatico de tipo supervisado

[VER_img:2.3-aprendizaje-supervisado]

Es de los mas utilizados hoy en dia. En este aprendizaje vamos a tener observaciones etiquetadas. Las observacion o datos que van a decir 'ok
esta dato significa que este mensaje es spam'. 

(1) Para ello el humano debe de etiquetar los datos 'este es spam, esto no lo es, ...' es decir, le decimos que es lo que esta bien, que es 
lo que esta mal. Que es un tipo A o que es un tipo B, que es un 'perro', que es un 'gato', etc.

(2) Luego dividimos nuestros datos, en un set de entrenamiento y en un set de pruebas.

(3) Luego estos datos de entrenamiento se los pasamos al algortimo ML, al algortimo que va a aprender automaticamente

(4) Por ultimo aplicamos nuestro modelo de prediccion. Luego hacemos las stats, es decir evaluamos que tan bien le fue a nuestro modelo.

Este es el flujo de trabajo tradicional de un aprendizaje supervisado.


# Aprendizaje automatico de tipo supervisado

Es totalmente diferente, ya que la computadora automaticamente va a ir detectando patrones en nuestros datos. Puede ser patrones en una img.

[VER_img:2.4-aprendizaje-no-supervisado]

Esto patrones les va a servir para agrupar los datos, en esto es una cara, esto es el resto de la informacion. Y con esto podemos hacer que 
las computadoras aprendan de forma autonoma, es decir no supervisadas.


# Algortimos mas utilizados en aprendizaje supervisado

[VER_img:2.5-regresion-clasificacion]

* [Regresion] : Es para detectar patrones. Como por ej. detectar el crecimiento en una poblacion, hacer una prediccion del clima como asi
tambien hacer una prediccion del mercado, hacer una proyeccion de ventas o de ingresos en una empresa. Todo lo que tenga que ver con agarrar
todo un set de datos y trazar una linea que nos va a decir cual va a hacer el comportamiento. Aqui puede haber [regresion-lineal] o 
[regresion-logistica].
Algunos algoritmos de regresion : regresion-lineal, regresion-logistica, naive-bayes etc.

* [Clasificacion] : Ej. Clasificar clientes para saber que cliente es un cliente premium, que cliente es el que esta en peligro de cancelar
el servicio, que cliente es el que gasta poquito. Otro ejemplo puede ser de diagnostico. Que persona es una posible persona con diabetes, 
que persona no, etc. Lo mismo puede hacerse para las imagenes 'este es un auto, este no lo es'.
Algunos algoritmos de clasificacion : k-nearest-neighbors, decision-trees, random-forest, etc.

[VER_img:2.6-algortimos-regresion-clasificacion]

A la Izq. son algoritmos de regresion y a la Der. algoritmos de clasificacion.

# *******************************************************************************************************************************************