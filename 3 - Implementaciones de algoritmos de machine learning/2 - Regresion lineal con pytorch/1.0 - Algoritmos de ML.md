
# *******************************************************************************************************************************************


# Recordemos : ¿ Que es regresion ?

Cuando usamos regresión, el resultado es un número. Es decir, el resultado de la técnica de machine learning que estemos usando será un 
valor numérico, dentro de un conjunto infinito de posibles resultados.

Aquí van algunos ejemplos de regresión:

    * Predecir por cuánto se va a vender una propiedad inmobiliaria
    * Predecir cuánto tiempo va a permanecer un empleado en una empresa
    * Estimar cuánto tiempo va a tardar un vehículo en llegar a su destino
    * Estimar cuántos productos se van a vender


# Algoritmos de ML (machine learning)

* Recordemos que hay multiples algoritmos de Machine Learning. Aqui unas cuantas

[VER-1.1-Algoritmo-de-ML]

* Algoritmo de regresion lineal : regresion
* Algoritmo de regresion logistica : Nos sirve mas para clasificaciones.
* Algoritmo Naive Bayer : Nos sirve para clasificar
* Algoritmo K-nearest neighbors : Nos sirve tanto para regresion como para clasificar.
* Algoritmo Decision tree : regresion y clasificacion
* Algoritmo Random Forest : regresion y clasificacion

*   Hay varias técnicas o algoritmos de machine learning que podemos usar en problemas de Regresión. Podemos destacar:

    regresión lineal y regresión no lineal
    máquinas de vectores de soporte (support vector machines)
    árboles de decisión (decision trees)
    bosques aleatorios (random forests)
    redes neuronales y aprendizaje profundo (deep learning)

Aunque hay algunas técnicas que son específicas de clasificación y otras de regresión, la mayoría de las técnicas funcionan con ambos.

Un motivo de confusión frecuente es la técnica de regresión logística. Su nombre podría inducirnos a pensar que puede usarse en problemas 
de regresión. Sin embargo, la regresión logística, sólo funciona para problemas de clasificación. La explicación completa está aquí.


# *******************************************************************************************************************************************